package com.example.ituneapi.network.model;


import com.google.gson.annotations.SerializedName;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;


public class ItuneAPI {

    String trackName;
    String artistName;
    String primaryGenreName;
    String releaseDate;

    public String getTrackName() {
        return trackName;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @SerializedName("artworkUrl100")
    String artworkUrl100;

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    @SerializedName("resultCount")
            private int resultCount;

    @SerializedName("results")
            private List<Result> results;

    public int getResultCount() {
        return resultCount;
    }

    public List<Result> getResults() {
        return results;
    }

    class Result{

    }
}

