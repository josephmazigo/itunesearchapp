package com.example.ituneapi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import com.example.ituneapi.R;
import com.example.ituneapi.network.model.ItuneAPI;
import com.example.ituneapi.network.model.ItuneResult;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {
    private Context context;

    private List<ItuneAPI> searchList;
    private SearchAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView trackName, artistName, primaryGenreName, releaseDate;
        public ImageView artworkUrl100;

        public MyViewHolder(View view) {
            super(view);
            trackName = view.findViewById(R.id.trackName);
            artistName = view.findViewById(R.id.artistName);
            primaryGenreName = view.findViewById(R.id.primaryGenreName);
            releaseDate = view.findViewById(R.id.releaseDate);
            artworkUrl100 = view.findViewById(R.id.artworkUrl100);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected callback
                    listener.onSearchSelected(searchList.get(getAdapterPosition()));
                }
            });
        }
    }


    public SearchAdapter(Context context, List<ItuneAPI> searchList, SearchAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.searchList = searchList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ItuneAPI ituneapi = searchList.get(position);

        holder.trackName.setText(ituneapi.getTrackName());
        holder.artistName.setText(ituneapi.getArtistName());
        holder.primaryGenreName.setText(ituneapi.getPrimaryGenreName());
        holder.releaseDate.setText(ituneapi.getReleaseDate());

        Glide.with(context)
                .load(ituneapi.getArtworkUrl100())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.artworkUrl100);
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public interface SearchAdapterListener {
        void onSearchSelected(ItuneAPI ituneapi);
    }
}
