package com.example.ituneapi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import com.example.ituneapi.R;
import com.example.ituneapi.network.model.ItuneAPI;

public class SearchAdapterFilterable extends RecyclerView.Adapter<SearchAdapterFilterable.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<ItuneAPI> searchList;
    private List<ItuneAPI> searchListFiltered;

    private SearchAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView trackName, artistName, primaryGenreName, releaseDate;
        public ImageView artworkUrl100;

        public MyViewHolder(View view) {
            super(view);
            trackName = view.findViewById(R.id.trackName);
            artistName = view.findViewById(R.id.artistName);
            primaryGenreName = view.findViewById(R.id.primaryGenreName);
            releaseDate = view.findViewById(R.id.releaseDate);
            artworkUrl100 = view.findViewById(R.id.artworkUrl100);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onSearchSelected(searchListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }


    public SearchAdapterFilterable(Context context, List<ItuneAPI> searchList, SearchAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.searchList = searchList;
        this.searchListFiltered = searchList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ItuneAPI ituneapi = searchListFiltered.get(position);

        holder.trackName.setText(ituneapi.getTrackName());
        holder.artistName.setText(ituneapi.getArtistName());
        holder.primaryGenreName.setText(ituneapi.getPrimaryGenreName());
        holder.releaseDate.setText(ituneapi.getReleaseDate());

        Glide.with(context)
                .load(ituneapi.getArtworkUrl100())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.artworkUrl100);
    }

    @Override
    public int getItemCount() {
        return searchListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    searchListFiltered = searchList;
                } else {
                    List<ItuneAPI> filteredList = new ArrayList<>();
                    for (ItuneAPI row : searchList) {


                        // searc keyword
                        if (row.getArtistName().toLowerCase().contains(charString.toLowerCase()) || row.getTrackName().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    searchListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = searchListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                searchListFiltered = (ArrayList<ItuneAPI>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface SearchAdapterListener {
        void onSearchSelected(ItuneAPI ituneapi);
    }
}