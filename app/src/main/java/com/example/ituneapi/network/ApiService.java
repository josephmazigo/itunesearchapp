package com.example.ituneapi.network;

import com.example.ituneapi.network.model.ItuneAPI;
import com.example.ituneapi.network.model.ItuneResult;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("search")
    Single<List<ItuneAPI>> getsearches(@Query("source") String source, @Query("term") String query);

    @GET("lookup")
    public void gettrack(@Query("source") String source, @Query("id") String query);

}
